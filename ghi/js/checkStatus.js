const payloadCookie = await cookieStore.get('jwt_access_payload');
if (payloadCookie) {
  let encodedPayload = payloadCookie.value;
  try {
    encodedPayload = JSON.parse(payloadCookie.value);
  } catch (e) { /* Do nothing if it's not JSON encoded */ }

  const payload = JSON.parse(atob(encodedPayload));
  const permissions = payload.user.perms;
  if (permissions.includes("events.add_conference")) {
    const link = document.querySelector("[href='new-conference.html']");
    if (link) {
      link.classList.remove('d-none');
    }
  }
  if (permissions.includes("events.add_location")) {
    const link = document.querySelector("[href='new-location.html']");
    if (link) {
      link.classList.remove('d-none');
    }
  }
}

// // Get the cookie out of the cookie store
// const payloadCookie = // FINISH THIS
// if (payloadCookie) {
//   // The cookie value is a JSON-formatted string, so parse it
//   const encodedPayload = JSON.parse(payloadCookie.value);

//   // Convert the encoded payload from base64 to normal string
//   const decodedPayload = // FINISH THIS

//   // The payload is a JSON-formatted string, so parse it
//   const payload = // FINISH THIS

//   // Print the payload
//   console.log(payload);

//   // Check if "events.add_conference" is in the permissions.
//   // If it is, remove 'd-none' from the link


//   // Check if "events.add_location" is in the permissions.
//   // If it is, remove 'd-none' from the link

// }
