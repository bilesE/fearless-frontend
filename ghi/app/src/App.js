import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import MainPage from './MainPage';

import { BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="attendees">
          <Route index element={<AttendeesList />} />
          <Route path="new" element={<AttendConferenceForm/>}/>
        </Route>
        <Route path="locations">
          {/* <Route index element={<LocationList />} /> */}
          <Route path="new" element={<LocationForm/>} />
        </Route>
        <Route path="conferences">
          {/* <Route index element={<ConferencesList />} /> */}
          <Route path="new" element={<ConferenceForm/>} />
        </Route>
        <Route path="presentation">
          {/* <Route index element={<PresentationList />} /> */}
          <Route path="new" element={<PresentationForm/>}/>
        </Route>
        </Routes>
        </div>
      </BrowserRouter>
);
}

export default App;
