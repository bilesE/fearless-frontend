import React, {useState, useEffect} from "react";

function ConfGoSearch() {
    const [searchName, setSearchName] = useState("")
    function handleNameChange(event) {
        setSearchName(event.target.value)
    }
    return <>
    <div className="container">
    <h1>
        <form>
            <input onChange={handleNameChange} placeholder="Name" id="name" name="name" type="text" className=""></input>
            {/* // basically place all your form ocntents here */}
        </form>
    </h1>
    </div>
    </>
}
// can be placed in app.js for realtime eval
